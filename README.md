# PARKDALE CENTRE DEVELOPMENT WORKSHOP

Welcome to Parkdale Centre for Innovation

This is the repository for the react development workshop.

The workshop consists of a React App that fetches information from an external API, parses it, and renders a view with it's contents.

We'll be using TVMaze API in order to retrieve TV Shows' information.

The UI contains a Search input field, a "Search" button that will fetch information from all the TV Series that match the text typed by the user.
The fetched information will then be parsed and shown using a mapping function.

## Online reference

### TVMaze API Docs

http://www.tvmaze.com/api

### Reactjs

https://reactjs.org/

### Using Fetch

https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

### Mapping

https://reactjs.org/docs/lists-and-keys.html

## Installation instructions

### NodeJS

- Installing node - [https://nodejs.org/en/](https://nodejs.org/en/)

### Git

- Windows - [https://git-scm.com/download/win](https://git-scm.com/download/win)
- Mac - Already installed
- Linux - Already installed

### Code Editor

- VS Code - [https://code.visualstudio.com/](https://code.visualstudio.com/)

### Base project

    git clone https://gitlab.com/moymac/react-workshop.git
    cd react-course
    npm install
    npm start

Open chrome and go to: [http://localhost:3000](http://localhost:3000).

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
