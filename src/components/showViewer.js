import React, { Component } from "react";
import OneShow from "./oneShow";
const ImageNotFoundURL =
  "https://vignette.wikia.nocookie.net/creepypasta/images/a/a6/Image-not-found.gif/revision/latest?cb=20131230041441";
export default class ShowViewer extends Component {
  render() {
    return (
      <div>
        {this.props.seriesData.map((item, index) => (
          <OneShow
            key={index}
            name={item.show.name}
            runtime={item.show.runtime}
            language={item.show.language}
            image={item.show.image ? item.show.image.medium : ImageNotFoundURL}
          />
        ))}
      </div>
    );
  }
}
