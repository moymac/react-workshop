import React, { Component } from "react";
export default class OneShow extends Component {
  render() {
    return (
      <div>
        <h1>{this.props.name}</h1>
        <img alt={this.props.name} src={this.props.image} />
        <h3>Runtime: {this.props.runtime}</h3>
        <h3>language: {this.props.language}</h3>
      </div>
    );
  }
}
