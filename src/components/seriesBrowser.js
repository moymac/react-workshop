import React, { Component } from "react";
import ShowViewer from "./showViewer";

const APIURL = " http://api.tvmaze.com/search/shows?q=";

export default class SeriesBrowser extends Component {
  constructor(props) {
    super(props);
    this.state = { seriesName: "", seriesInfo: [] };
  }
  onFormSubmit = async event => {
    event.preventDefault();
    const query = this.state.seriesName;
    let response = await fetch(APIURL + query);
    let jsonResponse = await response.json();
    console.log(jsonResponse);

    this.setState({ seriesInfo: jsonResponse });
  };
  handleInput = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({ [name]: value });
  };
  render() {
    return (
      <div>
        <form onSubmit={this.onFormSubmit}>
          <label>
            Search:
            <input
              name="seriesName"
              type="text"
              value={this.state.seriesName}
              onChange={this.handleInput}
            />
            <input type="submit" value="search" />
          </label>
        </form>
        <ShowViewer seriesData={this.state.seriesInfo} />
      </div>
    );
  }
}
