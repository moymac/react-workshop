import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import SeriesBrowser from "./components/seriesBrowser";

class App extends Component {
  render() {
    return (
      <div className="App">
        <SeriesBrowser />
      </div>
    );
  }
}

export default App;
